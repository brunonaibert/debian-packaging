# html2text 2.2.3

```bash
chmod 777 ~bruno/empacotamento/html2text
sudo docker run -it -v ~bruno/empacotamento/html2text:/PKG/html2text --name html2text-2.2.3 jaula-sid:20230909
apt update
apt dist-upgrade
cd /PKG/html2text
debcheckout -a html2text
cd html2text
git checkout pristine-tar
git checkout upstream
git checkout master
git branch -m debian/master
gbp export-orig
```

Devido a erro no arquivo `debian/watch` precisarei de uma alteração temporária para baixar a nova versão

Edite o arquivo `debian/watch`, deixando da seguinte forma:

```
version=4                                                                       
https://github.com/grobian/html2text/tags .*/tags/v?(\d\S+)\.tar\.(?:bz2|gz|xz)
```

```bash
uscan
git restore debian/watch
gbp import-orig ../html2text_2.2.3.orig.tar.gz
gbp dch
```

```bash
dch -r
```

1. Altere de UNRELEASED para unstable
2. Adicione ponto final em "* New upstream version 2.2.3."
3. Corrija ortografia do commit anterior: maintainer

```bash
git commit -a -m "New changelog"
git diff upstream/1.3.2a upstream/2.1.1 > /tmp/html2text-diff.diff
```

Abra o arquivo `/tmp/html2text-diff.diff`, visualize as alterações.

Anote arquivos excluídos, mudanças de licença, mudanças de nome, mudanças de local e arquivos novos.

```bash
egrep -sriA25 '(public dom|copyright)' --exclude-dir=debian > /tmp/html2text-copyright.txt
```

Abra o arquivo `/tmp/html2text-copyright.txt` para análise das licenças.

Realizando alguns trabalhos

Removendo arquivo desnecessário

```bash
git rm debian/README.source
```

Adicionada entrada ao `debian/changelog`:

```bash
dch -r
```

Adicionando entrada:

```
  * debian/README.source: removed because this file is useless now.
```

Commitando alteração:

```bash
git commit -a -m "Removing useless file debian/README.source"
```

Removendo customizações Debian:

```bash
git rm debian/README.Debian
git rm debian/patches/500-utf8-support.patch
git rm debian/patches/510-disable-backspaces.patch
git rm debian/patches/611-recognize-input-encoding.patch
git rm debian/patches/630-recode-output-to-locale-charset.patch
```

Edite o arquivo `debian/patches/series` para remover chamada ao patches removidos.

Adicionando informações ao `debian/NEWS`:

```bash
dch --news
```

Altere "UNRELEASED" para "unstable".

Conteúdo:
```
  Removed all customizations of Debian version.                                 
                                                                                
  Several changes made by new upstream. The source code is under active         
  development and features are managed by upstream.
```

Adicionando entrada ao `debian/changelog`:

```bash
dch -r
```

Conteúdo:
```
  * Removed all customizations of Debian version, because the source code is    
    under active development. Consequently:                                     
      - debian/NEWS: added information about disabled customizations of the     
        Debian version.                                                         
      - debian/patches removed:                                                 
        ~ 500-utf8-support.patch                                                
        ~ 510-disable-backspaces.patch                                          
        ~ 611-recognize-input-encoding.patch                                    
        ~ 630-recode-output-to-locale-charset.patch                             
      - debian/README.Debian: removed.
```

Commitando alterações:

```bash
git commit -a -m "Removing all customizations of Debian version"
```

TODO:

* Revisar rules
* Revisar licenciamento
* atualizar copy





-----------------------

Arquivos novos:

/.gitignore
/Area.cpp - ver licenciamento
/HISTORY
/HTMLControl.cpp - ver licenciamento
/HTMLDriver.cpp - ver licenciamento
/HTMLDriver.h - ver licenciamento
/HTMLParser.cc - ver licenciamento
/HTMLParser.hh - ver licenciamento
/Makefile.am - ver licenciamento
/Properties.cpp - ver licenciamento
/README.md - ver licenciamento
/cmp_nocase.cpp - ver licenciamento
/config.rpath - ver licenciamento
/configure.ac - ver licenciamento
/format.cpp - ver licenciamento
/html.cpp - ver licenciamento
/html2text.1 - ver licenciamento
/html2text.cpp - ver licenciamento
/html2textrc.5 - ver licenciamento
/iconvstream.cpp - ver licenciamento
/iconvstream.h - ver licenciamento
/istr.h - ver licenciamento
/m4/iconv.m4 - ver licenciamento
/release.sh - ver licenciamento
/sgml.cpp - ver licenciamento
/table.cpp - ver licenciamento
/tests/.html2textrc - ver licenciamento
/tests/Makefile - ver licenciamento
/tests/runtest.sh - ver licenciament

Arquivos removidos:

/1.3.2a_to_1.3.2.diff
/Area.C
/CHANGES
/HTMLControl.C
/HTMLParser.C
/HTMLParser.h
/HTMLParser.k
/Properties.C
/README
/RELEASE_NOTES
/TODO
/ascii.substitutes
/cmp_nocase.C
/format.C
/html.C
/html2text.C
/html2textrc.5.gz
/libstd/Makefile.in
/libstd/include/list
/libstd/include/map
/libstd/include/memory
/libstd/include/new
/libstd/include/set
/libstd/include/string
/libstd/include/utility
/libstd/include/vector
/libstd/rb_tree.C
/libstd/rb_tree.h
/libstd/string.C
/libstd/vector_base.C
/libstd/vector_base.h
/ls-alRF
/sgml.C
/table.C
/urlistream.C 
/urlistream.h


Modificado:

/INSTALL - Contém copyright
/auto_aptr.h

--- a/libstd/include/auto_ptr.h                                                 
+++ b/auto_ptr.h


--- a/pretty.style                                                              
+++ b/contrib/pretty.style

--- a/sgml.h                                                                    
+++ b/sgml.h

