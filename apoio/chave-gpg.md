# Chave GPG

Uma chave GPG é utilizada no empacotamento para assinatura de pacotes.

## Criar chave GPG RSA 4096

Criar chave GPG RSA 4096 bits (com usuário comum):

```bash
gpg --full-gen-key
```

Passos:

1. Selecione tipo da chave ppção 1 - RSA and RSA (default) 
2. Defina tamanho da chave, 4096
3. Selecione a validade da chave, 1y
4. Confirme seleção de expiração
5. Informe seu nome completo
6. Informe o seu e-mail
7. Deixe campo comentário em branco

## Gerar certificado de revogação

```bash
gpg --gen-revoke fingerprint > ~/.gnupg/revocation-fingerprint.crt
```

Observação: O fingerprint corresponde aos 8 últimos dígitos da identificação da chave.

Enviar chave pública para um servidor:

```bash
gpg --send-key fingerprint
```

Listar certificados presentes para usuário atual:

```bash
gpg --list-keys
```

Exportar chave pública (apara ser usada na jaula-sid)

```bash
cd ~/empacotamento
```

```bash
gpg -a --export fingerprint > ./chave.pub
```

Exportar chave privada

```bash
gpg -a --export-secret-keys fingerprint > ./chave.key
```

Obtendo informações da chave:

```bash
gpg -v --fingerprint fingerprint
```

Gerando impressão da chave para uma festa de assinatura de chaves:

```bash
apt update && apt install signing-party
gpg-key2ps -p a4 fingerprint > /home/bruno/festa-assinatura-chaves-fingerprint.ps
```

Ou gerando diretamente em PDF:

```bash
gpg-key2ps -p a4 fingerprint | gs -sDEVICE=pdfwrite \
 -sOutputFile=/home/bruno/festa-assinatura-chaves-fingerprint.pdf
```

Observação: Para se candidatar a DM ou DD, é necessário que sua chave GPG seja assinada pelo menos por dois DDs.
Dica: Criar um cartão de visitas com ID da chave impresso. No cartão também ter um QRCode com os dados no formato vCard.