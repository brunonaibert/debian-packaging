# Conhecendo a estrutura do pacote deb

Baixe um pacote para realizarmos seu estudo:

```bash
mkdir /tmp/funcoeszz
cd /tmp/funcoeszz
apt download funcoeszz
```

Os arquivos .deb são arquivos compactado com o comando "ar"

Podemos extrair o conteúdo com comando:

```bash
ar -x funcoeszz_21.1-1_all.deb
```

Versão do binário:

```bash
cat debian-binary
```

Arquivos que serão instalados:

```bash
tar -xvf data.tar.xz
```

Analisando informações do pacote:

```bash
tar -xvf control.tar.xz
cat control
```