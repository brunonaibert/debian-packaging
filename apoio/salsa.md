# Debian Salsa (GitLab)

## Criar chave SSH

Execute o comando abaixo (altere para corresponder ao seu e-mail):

```bash
ssh-keygen -t rsa -b 4096 -C "email"
```

## Configurar acesso ao Salsa

Adicione ao arquivo `~/.ssh/config`:

```
# Debian Salsa
Host salsa.debian.org
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_rsa
```