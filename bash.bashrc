alias ls='ls --color=auto'
alias tree='tree -aC'
alias debuildsa='dpkg-buildpackage -sa -knr_da_chave_gpg_completa'
alias uscan-check='uscan --verbose --report'
alias debcheckout='debcheckout -a'
export DEBFULLNAME="seu_nome_completo_sem_acentos_ou_cedilhas"
export DEBEMAIL="seu_e-mail"
export EDITOR=vim
export LANG=C.UTF-8
export LANGUAGE=C.UTF-8
export LC_ALL=C.UTF-8
export QUILT_PATCHES=debian/patches
export PS1='JAULA-SID-\u@\h:\w\$ '
cd /PKG