# Tutorial de empacotamento do Debian

Uma das muitas formas de empacotar software para o Debian.

Para documentação oficial, acesse [Canto dos(as) desenvolvedores(as) Debian][1]

## Pré-requisito

```bash
sudo apt update
sudo apt install docker.io

gpg -a --export nr_da_chave > ./chave.pub
gpg -a --export-secret-keys nr_da_chave > ./chave.key
```

## Instalação

```bash
mkdir ~/empacotamento
cd ~/empacotamento
wget https://salsa.debian.org/brunonaibert/debian-packaging/-/archive/main/debian-packaging-main.tar.gz
tar -zxf debian-packaging-main.tar.gz
cd debian-packaging-main
```

## Construção da jaula-sid

```bash
docker build -t jaula-sid:`date +%Y%m%d` .
```

## Índice

1. [Criar container Sid (unstable)](./docker-image-jaula-sid.md)
2. [Configurar acesso ao Salsa](./salsa.md)

## Material de apoio

* [Conhecendo a estrutura do pacote deb](./apoio/pacote-deb.md)

[1]: https://www.debian.org/devel/