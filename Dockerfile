FROM debian:sid

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

COPY ./bash.bashrc /etc/bash.bashrc
COPY ./.vimrc /root/.vimrc
COPY ./lintianrc /etc/lintianrc
COPY ./gbp.conf /tmp/gbp.conf
COPY ./chave.pub /tmp/chave.pub
COPY ./chave.key /tmp/chave.key

RUN ln -fs /usr/share/zoneinfo/America/Sao_Paulo  /etc/localtime
    && dpkg-reconfigure -f noninteractive tzdata
    && apt update && apt upgrade -y \
    && apt install -y --no-install-recommends \
    autopkgtest \
    blhc \
    devscripts \
    dh-make \
    dput-ng \
    git-buildpackage \
    how-can-i-help \
    locales \
    quilt \
    renameutils \
    pristine-tar \
    ssh \
    spell \
    splitpatch \
    tardiff \
    tree \
    wget \
    vim \
    && apt clean \
    && gpg --list-keys \
    && echo "pinentry-mode loopback" >> /root/.gnupg/gpg.conf \
    && gpg --batch --import /tmp/chave.key /tmp/chave.pub \
    && mkdir /PKG \
    && sed -i -- 's/Types: deb/Types: deb deb-src/g' /etc/apt/sources.list.d/debian.sources \
    && mv -f /tmp/gbp.conf /etc/git-buildpackage/gbp.conf \
    && rm /tmp/*